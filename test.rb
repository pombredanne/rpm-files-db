#!/usr/bin/ruby

require 'rubygems'
require 'nokogiri'


Xml = <<-XML
<?xml version="1.0" encoding="ISO-8859-1"?>
<package name="ruby">
<file type="dir">/usr/bin</file>
<file>/usr/bin/ruby</file>
<file>/usr/bin/irb</file>
<file type="dir">/usr/doc/ruby</file>
<file>/usr/doc/ruby/index.html</file>
</package>

<package name="perl">
<file type="dir">/usr/bin</file>
<file>/usr/bin/perl</file>
<file>/usr/bin/cpan</file>
<file type="dir">/usr/doc/perl</file>
<file>/usr/doc/perl/index.html</file>
</package>
XML

doc = Nokogiri::HTML(Xml)
puts doc
puts doc.xpath("//file[@type != \"dir\"]")
puts doc.xpath("//file[not (@type)]")

puts "\n\npkg"
pkg = doc.at_xpath("//package")
puts pkg
puts pkg.xpath("file[not (@type)]").text
