#!/usr/bin/ruby1.9

require 'rubygems'
require 'sqlite3'
require 'optparse'
require 'ostruct'

#----------------------------------------------------------------------------
#       repository control
#----------------------------------------------------------------------------
DbDir = "#{Dir.home}/lib/rpmfiles"

class Repo
    attr_reader :name, :file
    def initialize(file)
#         @name = /rpmfiles-(.+)\.db/.match( File.basename(file) )[1]
#         @file = file
        @name = File.basename(file)
        @file = file
    end

    def self.readRepos
        [ Repo.new( File.join(DbDir, "rpmfiles.db") ) ]
#         Dir[File.join(DbDir, "rpmfiles-*.db")].map { |f| Repo.new(f) }
    end

    def self.match(name)
        return self.repos
        
        re = Regexp.new( Regexp.quote(name), true )
        matches = self.repos.select { |r| re.match(r.name) }
        matches.each do |r|
            return [r] if r.name.size == name.size
        end
        matches
    end

    def self.matchOne(name)
        matches = self.match(name)
        if matches.size == 0 then
            matches[0]
        else
            nil
        end
    end

    def self.repoNames
        self.repos.map { |r| r.name }
    end

    def self.repos
        @@repos ||= readRepos
    end
end

def listRepos
    Repo.repoNames.each { |r| puts r }
end


#----------------------------------------------------------------------------
#       database control
#----------------------------------------------------------------------------


def getRpmNameById(id)
    sqlq = <<-SQLq
        select full_name from rpmnames where id is #{id}
    SQLq
    @db.get_first_value(sqlq)
end

def getRpmById(id)
    sqlq = <<-SQLq
        select full_name, repo_id from rpmnames where id is '#{id}'
    SQLq
    @db.get_first_row(sqlq)
end

def getRepoNameById(id)
    def getFromDb(id)
        sqlq = <<-SQLq
            select name from reponames where id is '#{id}'
        SQLq
        @db.get_first_value(sqlq)
    end
    @repoNameTbl ||= Hash.new
    @repoNameTbl[id] ||= getFromDb(id)
end

#----------------------------------------------------------------------------
#       program start
#----------------------------------------------------------------------------

#----------------------------------------------------------------------------
#       options
#----------------------------------------------------------------------------

# set default option parameters
@options = OpenStruct.new
@options.verbose = false
@options.command = nil

# option parsing
pname = File.basename($PROGRAM_NAME)
opts = OptionParser.new do |o|
        o.banner = <<-EOF
find file from rpm.
Usage: #{pname} [options] [repository_name] file_name
options :
        EOF
        o.on("-v", "--verbose", "verbose mode.") do |f|
            @options.verbose = true
        end
        o.on("-l", "--list-repo", "list repositories.") do |f|
            @options.command = :listRepos
        end
    end

begin
    opts.parse!(ARGV)
rescue OptionParser::InvalidOption
    puts opts
    exit 0
end

case @options.command
when :listRepos
    puts "all repositories."
    listRepos
    exit 0
end


repo = "update"

case ARGV.size
when 0
    puts opts
    exit 1
when 1
    @searchWord = ARGV.shift
when 2
    repo = ARGV.shift
    @searchWord = ARGV.shift
end


matches = Repo.match(repo)
case matches.size
when 0
    puts "no matched repositories."
    puts "all searchable repositories."
    listRepos
    exit 1
when 1
    @repo = matches[0]
else
    puts "more than one mathed repo."
    puts "matched repos."
    matches.each { |r| puts r.name }
    exit 1
end


#----------------------------------------------------------------------------
#       main
#----------------------------------------------------------------------------


@db = SQLite3::Database.new(@repo.file)
sql = <<-SQL
select id, file_name from rpmfiles where base_name match '%#{@searchWord}'
SQL

# puts "query is '#{sql}'"
res = @db.execute(sql)
# p res
res.each do |row|
    id = row[0]
    rpmName, repoId = getRpmById(id)
    repoName = getRepoNameById(repoId)
    puts "%-40s : %s" % [ rpmName, repoName ]
    puts "   " +row[1]
end

